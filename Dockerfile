# Specifies the image of your engine
FROM node:14-alpine

# The working directory inside your container
WORKDIR /srv/app

# Get the package.json first to install dependencies
COPY package.json package-lock.json /
RUN npm install
COPY . ./

# Run the container
CMD ["npm", "run", "dev"]

EXPOSE 8080
