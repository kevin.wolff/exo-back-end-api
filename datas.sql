-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : db
-- Généré le : jeu. 13 mai 2021 à 22:04
-- Version du serveur :  10.5.9-MariaDB-1:10.5.9+maria~focal
-- Version de PHP : 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `tracker`
--

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

DROP TABLE `ticket`;
CREATE TABLE `ticket` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `name` text NOT NULL,
  `status` int(11) NOT NULL,
  `index` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ticket`
--

INSERT INTO `ticket` (`id`, `description`, `name`, `status`, `index`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Front - responsive navbar', 1, 1),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Front - meta tag et SEO', 1, 1),
(3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Back - revoir endpoints error statut', 1, 1),
(4, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Back - durée de stockage des cookies', 1, 1),
(5, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Dev ops - update dépendances', 1, 1),
(6, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Back - ajout de fonctionnalités au formulaire de création d\'un ticket', 1, 1),
(7, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Front - revoir balisage des labels formulaire', 1, 1),
(8, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Etude front - mise en plage d\'un mode nocturne', 1, 1),
(9, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Front - smoove scrool page d\'accueil', 1, 1),
(10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Back - nouveau endpoint api methode get', 1, 1),
(11, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Etude back - faire un rapport sur sur le modul websecu', 1, 1),
(12, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Front - mise en place du drag and drop', 1, 1),
(13, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Back - revoir contraintes de données', 1, 1),
(14, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Dev ops - tester deploy stagging pour test client', 1, 1),
(15, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Back - Afficher détails d\'un ticket au clique', 1, 1),
(16, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Front - accessibilité navigation clavier', 1, 1),
(17, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Back - test web security cross origin and forge origin', 1, 1),
(18, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Front - test fonctionnel création d\'un ticket ', 1, 1),
(19, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Front - mettre en place animation sidebar et mobile menu', 1, 1),
(20, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Quae diligentissime contra Aristonem dicuntur a Chryippo. ', 'Back - methode delete pour tickets archivés', 1, 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
