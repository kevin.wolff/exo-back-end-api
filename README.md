# Tracker - projet Simplon

Projet back-end réalisé par [Baptiste](https://gitlab.com/azdra), [Jean](https://gitlab.com/jean.mionnet.simplon), [Kévin](https://gitlab.com/kevin.wolff), [Lisa](https://gitlab.com/lmichallon).

## Installation avec Docker et docker-compose

### Pré-requis

Assurez-vous d'avoir Docker et docker-compose d'installé sur votre système.

Selon le site de [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) assurez-vous d'avoir votre utilisateur actuel dans le groupe Docker.

```shell
sudo groupadd docker
sudo usermod -aG docker ${USER}
sudo newgroup docker
```

## Installation du projet

```shell
git clone https://gitlab.com/jean.mionnet.simplon/tracker
cp .env.dist .env
npm install
```

Lancer Docker.

```
docker-compose up
```

Executer le script SQL présent dans le projet.

```
cat ./datas.sql | docker-compose exec -T db mysql -u tracker -pJwQyPx0mIaPAy93K tracker
```

Accéder à l'API :

http://localhost:8000/tickets