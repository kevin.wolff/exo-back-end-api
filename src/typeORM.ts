import 'reflect-metadata'
import { createConnection } from 'typeorm'
import { TicketEntity } from './Entity/Ticket'
import { App } from './app'

export default function typeORM() {

    createConnection({
        type: 'mysql',
        url: process.env.ORM_URL,
        entities: [
            TicketEntity
        ],
        synchronize: true,
        logging: false
    }).then(connection => {

        const ticketRepository = connection.getRepository(TicketEntity);

        App.get('/tickets', async (req, res) => {
            const tickets = await ticketRepository.find()

            if (tickets) {
                return res.status(200).json(tickets)
            } else {
                return res.status(404).send()
            }
        })

        App.get('/tickets/:id', async (req, res) => {
            const ticket = await ticketRepository.findOne(req.params.id)

            if (ticket) {
                return res.status(200).send(ticket)
            } else {
                return res.status(404).send()
            }
        })

        App.post('/tickets', async (req, res) => {
            try {
                const ticket = await ticketRepository.create(req.body)
                const results = await ticketRepository.save(ticket)
                return res.status(200).send(results)
            } catch {
                return res.status(400).send()
            }
        })

        App.put('/tickets/:id', async (req, res) => {
            try {
                const ticket = await ticketRepository.findOne(req.params.id)
                ticketRepository.merge(ticket, req.body)
                const results = await ticketRepository.save(ticket)
                return res.status(200).send(results)
            } catch {
                return res.status(400).send()
            }
        })

        App.delete('/tickets/:id', async (req, res) => {
            const ticket = await ticketRepository.findOne(req.params.id)
            if (ticket) {
                const tickets = await ticketRepository.delete(req.params.id)
                return res.status(200).send(tickets)
            } else {
                return res.status(404).send()
            }
        })
    })
        .catch(error => console.log('Error connecting to DB', error))
}