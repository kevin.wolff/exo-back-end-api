import * as express from 'express'
import { Express } from "express"
const App: Express = express()
export { App }

import typeORM from './typeORM'
typeORM()

// START EXPRESS SERVER
App.listen(8000, () => {
  console.log('API available at http://localhost:%s/tickets', process.env.NODE_PORT)
  console.log('phpMyAdmin available at http://localhost:%s', process.env.PMA_PORT)
  console.log(`phpMyAdmin ID username: %s`, process.env.DB_NAME)
  console.log(`phpMyAdmin ID password: %s`, process.env.DB_PASSWORD)
});